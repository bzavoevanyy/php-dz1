<!--1. Создайте массив $bmw с ячейками:-->
<!--a. model-->
<!--b. speed-->
<!--c. doors-->
<!--d. year-->
<!--2. Заполните ячейки значениеями соответвенно: “X5”, 120, 5, “2015”-->
<!--3. Создайте массивы $toyota и $opel аналогичные массиву $bmw (заполните-->
<!--данными)-->
<!--4. Выведите значения всех трех массиво в виде:-->
<!--name ­ model ­speed ­ doors ­ year-->
<!--Например:-->
<!--X5 ­120 ­ 5 ­ 2015-->
<!--5. Для закрепления изученного материала придумайте аналогичную задачу,-->
<!--желательно, немного усложнив ее. Напишите текст задачи и пример ее решения-->
<?php
$bmw = ['X5',120,5, 2015];
$toyota = ['Land Cruiser', 200, 5, 2014];
$opel = ['Mokka', 180, 5, 2013];
$count = true;

foreach($bmw as $value) {
    if ($count) echo $value;
    else echo ' - '.$value;
    $count = false;
}
$count = true;
echo '<br>';
foreach($toyota as $value) {
    if ($count) echo $value;
    else echo ' - '.$value;
    $count = false;
}
$count = true;
echo '<br>';
foreach($opel as $value) {
    if ($count) echo $value;
    else echo ' - '.$value;
    $count = false;
};

// Создать массив из автомобилей и меременную - фильтр года выпуска. Выводить список автомобилей в зависимости от
// значения фильта.


echo '<br>------------------------<br>';
$year = 2007;
$cars = [
    'bmw' => ['X5', 120, 5, 2005],
    'audi' => ['A4', 160, 4, 2007],
    'toyota' => ['RAV4', 180, 5, 2009],
    'kia' => ['rio', 160, 4, 2011],
    'honda' => ['akkord', 200,4,2012]
];
foreach($cars as $key => $value) {
    if ($value[3] >= $year) {
        echo $key;
        foreach ($value as $val) {
            echo ' - '.$val;
        }
        echo '<br>';
    }
}