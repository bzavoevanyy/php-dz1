<!--1. Используя цикл for выведите таблицу умножения размером 5 строк на 6 столбцов.-->
<!--2. Для закрепления изученного материала придумайте аналогичную задачу,-->
<!--желательно, немного усложнив ее. Напишите текст задачи и пример ее решения-->
<?php
$first = true;
$second = true;
echo '<table style border="1px solid grey">';
for ($y=0;$y<6;$y++) {
    echo '<tr>';
    if (!$first) echo '<td style="color: red;">'.$y.'</td>';
    for($x=1;$x<7;$x++) {
        if ($first) {
            echo '<td>'.' '.'</td>';
            echo '<td style="color: red">'.$x.'</td>';
        } elseif ($second) {
            echo '<td style="color: red">'.$x.'</td>';
        } else echo '<td>'.$x * $y.'</td>';
        $first = false;
    }
    $second = false;
    echo '</tr>';
}
echo '</table>';

// Отрисовать шахматную доску

$black = '<td style="width: 50px; height: 50px; background-color: #000"></td>';
$white = '<td style="width: 50px; height: 50px; background-color: #fff"></td>';
echo '<table style="border: 1px solid grey">';
for ($y=1; $y<9;$y++) {
    echo '<tr>';
    for ($x=1;$x<9;$x++) {
        if ($y % 2 == 0) {
            if ($x % 2 == 0) {
                echo $white;
            } else echo $black;
        } else {
            if ($x % 2 == 0) {
                echo $black;
            } else echo $white;
        }
    }
    echo '</tr>';
}