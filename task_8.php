<!--Отрисовать навигационное меню на странице, типа <a href=’about.html’>О нас</a>-->
<!--используюя массив в качетсве структуры меню-->
<!--1. Создайте ассоциативный массив $menu-->
<!--2. Заполните массив соблюдая следующие условия, например:-->
<!--a. навзание ячейки является пунктом меню, например: Номе, About, Contacts…-->
<!--b. Значение ячейки является именем файла, на который буедт указывать-->
<!--ссылка, например: index.php, about.php, contacts.php-->
<!--3. Используя цикл foreach отрисуйте ветрикальное меню.-->
<!--4. Для закрепления изученного материала придумайте аналогичную задачу,-->
<!--желательно, немного усложнив ее. Напишите текст задачи и пример ее решения-->
<?php
$menu = [
    'Home' => 'index.php',
    'About' => 'about.php',
    'Contats' => 'contacts.php'
];
echo '<ul>';
foreach ($menu as $key => $value) {
    echo '<li>';
    echo '<a href='.$value.'>'.$key.'</a>';
}
echo '</ul>';

// Сделать меню из 5-ти элементов, отрисовать его. Если значение переменной $vertical = true - меню вертикальное,
// если $vertical = false - горизонтальное;

$menu = [
    'elem1' => 'index.php',
    'elem2' => 'about.php',
    'elem3' => 'contact.php',
    'elem4' => 'shop.php',
    'elem5' => 'forum.php'
];
$vertical = false;
echo '<ul>';
foreach ($menu as $key => $value) {
    if ($vertical) echo '<li>';
    else echo '<li style="display: inline-block; margin-left: 5px;">';
    echo '<a href='.$value.'>'.$key.'</a>';
}